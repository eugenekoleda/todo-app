import React from "react";

import "../../common/common.css";
import "./ToDoList.css";
import TodoListItem from "../TodoListItem";
import ItemFilter from "../ItemFilter";

const ToDoList = ({ items, deleteItem, clearAll, onToggleImportant, onToggleDone, filter, onFilterChange}) => {
    let doned = 0;
    items.forEach(el => {
      if (el.done) {
        doned++;
      }
    });
    const toDo = items.length - doned;
    return (
      <div className="todo-list">
        <span className="doned">
          {toDo} more to do, {doned} done
        </span>
        <input type="checkbox" id="toggle-list" className="toggle-list" />
        <label
          htmlFor="toggle-list"
          className="toggle-list-label"
        >
          &#11167;
        </label>
        <ul>
          {items.map(el => (
            <li key={el.id}>
              <TodoListItem
                item={el}
                deleteItem={() => deleteItem(el.id)}
                onToggleImportant={() => onToggleImportant(el.id)}
                onToggleDone={() => onToggleDone(el.id)}
              />
            </li>
          ))}
        </ul>
        <div className="todo-footer">
          <ItemFilter filter={filter} onFilterChange={onFilterChange} />
          <button className="clear-btn" onClick={clearAll}>
            Clear
          </button>
        </div>
      </div>
    );
};

export default ToDoList;
