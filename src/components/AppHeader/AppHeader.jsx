import React from "react";

import "./AppHeader.css";

const AppHeader = () => {
  return <h1 className="app-header">To Do List</h1>;
};

export default AppHeader;
