import React, { Component } from "react";

import ToDoList from "../ToDoList";
import AppHeader from "../AppHeader";
import SearchPanel from "../SearchPanel";
import CreateItem from "../CreateItem";

import "./App.css";

export default class App extends Component {
  state = {
    items: localStorage.getItem("items")
      ? JSON.parse(localStorage.getItem("items"))
      : [
          {
            label: "Hello",
            id: 0,
            important: false,
            done: false
          }
        ],
    term: "",
    filter: "all"
  };
  componentDidMount() {
    localStorage.setItem("items", JSON.stringify(this.state.items));
  }
  onToggleImportant = id => {
    this.setState(state => {
      const idx = this.state.items.findIndex(el => el.id === id);
      const newItem = {
        ...this.state.items[idx],
        important: !state.items[idx].important
      };
      const newItems = [
        ...this.state.items.slice(0, idx),
        newItem,
        ...this.state.items.slice(idx + 1)
      ];
      localStorage.setItem("items", JSON.stringify(newItems));
      return {
        items: newItems
      };
    });
  };
  onToggleDone = id => {
    this.setState(state => {
      const idx = this.state.items.findIndex(el => el.id === id);
      const newItem = {
        ...this.state.items[idx],
        done: !state.items[idx].done
      };
      const newItems = [
        ...this.state.items.slice(0, idx),
        newItem,
        ...this.state.items.slice(idx + 1)
      ];
      localStorage.setItem("items", JSON.stringify(newItems));
      return {
        items: newItems
      };
    });
  };
  addItem = label => {
    const newItem = {
      label,
      id: parseInt(Math.random() * 1000),
      important: false,
      done: false
    };
    this.setState(state => {
      const newItems = [...state.items, newItem];
      localStorage.setItem("items", JSON.stringify(newItems));
      return {
        items: newItems
      };
    });
  };
  deleteItem = id => {
    this.setState(({ items }) => {
      const idx = items.findIndex(el => el.id === id);
      const newItems = [...items.slice(0, idx), ...items.slice(idx + 1)];
      localStorage.setItem("items", JSON.stringify(newItems));
      return {
        items: newItems
      };
    });
  };
  clearAll = () => {
    localStorage.setItem("items", JSON.stringify([]));
    this.setState({
      items: []
    });
  };
  search = (items, term) => {
    return term
      ? items.filter(
          el => el.label.toLowerCase().indexOf(term.toLowerCase()) > -1
        )
      : items;
  };
  filter = (items, filter) => {
    switch (filter) {
      case "all":
        return items;
      case "active":
        return items.filter(el => !el.done);
      case "done":
        return items.filter(el => el.done);
      default:
        return items;
    }
  };
  changeTerm = term => {
    this.setState({
      term
    });
  };
  onFilterChange = filter => {
    this.setState({
      filter
    });
  };
  render() {
    const { items, term, filter } = this.state;
    const visibleItems = this.filter(this.search(items, term), filter);
    return (
      <div className="app">
        <AppHeader />
        <CreateItem addItem={this.addItem} />
        <ToDoList
          items={visibleItems}
          deleteItem={this.deleteItem}
          clearAll={this.clearAll}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone}
          filter={filter}
          onFilterChange={this.onFilterChange}
        />
        <SearchPanel changeTerm={this.changeTerm} />
      </div>
    );
  }
}
