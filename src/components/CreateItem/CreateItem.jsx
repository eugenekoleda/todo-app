import React, { Component } from "react";

import "../../common/common.css";
import "./CreateItem.css";

export default class CreateItem extends Component {
  state = {
    label: ""
  };
  onLabelChange = event => {
    this.setState({
      label: event.target.value
    });
  };
  onSubmit = event => {
    event.preventDefault();
    this.props.addItem(this.state.label);
    this.setState({
      label: ""
    });
  };
  render() {
    return (
      <form className="create-item" onSubmit={this.onSubmit}>
        <input
          className="create-item__input"
          placeholder="new task..."
          value={this.state.label}
          onChange={this.onLabelChange}
        />
        <button type="submit" className="create-item__btn">
          &#xe949;
        </button>
      </form>
    );
  }
}
