import React, { Component } from "react";

import "../../common/common.css";
import "./SearchPanel.css";

export default class SearchPanel extends Component {
  state = {
    label: "",
  };
  onLabelChange = (event) => {
      this.props.changeTerm(event.target.value);
      this.setState({
          label: event.target.value,
      });
  };
  render() {
    return (
      <form className="search-panel" onSubmit={event => event.preventDefault()}>
        <input className="search-panel__input" value={this.state.label} placeholder="search" onChange={this.onLabelChange} />
        <button type="submit" className="search-panel__btn">
          &#xe986;
        </button>
      </form>
    );
  }
}
