import React from "react";

import "../../common/common.css";
import "./TodoListItem.css";

const TodoListItem = ({ item, deleteItem, onToggleImportant, onToggleDone }) => {
    const { important, done } = item;
    let classNames = "list-item";
    if (important) {
      classNames += " important";
    }
    if (done) {
      classNames += " done";
    }
    return (
      <span className="list-item-row">
        <span className={classNames} onClick={onToggleDone}>
          {item.label}
        </span>
        <span className="buttons">
          <button className="important-btn" onClick={onToggleImportant}>
            &#xe900;
          </button>
          <button className="delete-btn" onClick={deleteItem}>
            &#xe9ad;
          </button>
        </span>
      </span>
    );
}

export default TodoListItem;
